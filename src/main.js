import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins'
import vuetify from './plugins/vuetify'
import "vuetify/dist/vuetify.min.css";
import { sync } from 'vuex-router-sync'
import axios from 'axios'

import VueAxios from 'vue-axios'
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css'
import * as VueGoogleMaps from 'vue2-google-maps'
// see docs for available options


const datepickerOptions = {
    sundayFirst: false,
    dateLabelFormat: 'DD-MM-YYYY',
    days: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'],
    daysShort: ['L', 'M', 'M', 'J', 'V', 'S', 'D'],
    monthNames: [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Augosto',
      'Septiembre',
      'Octobre',
      'Noviembre',
      'Diciembre',
    ],
    colors: {
      selected: '#00a699',
      inRange: '#66e2da',
      selectedText: '#fff',
      text: '#565a5c',
      inRangeBorder: '#33dacd',
      disabled: '#fff',
      hoveredInRange: '#67f6ee'
    },
    texts: {
      apply: 'Apply',
      cancel: 'Cancel',
      keyboardShortcuts: 'Keyboard Shortcuts',
    },
   
    keyboardShortcuts: [
      {symbol: '↵', label: 'Select the date in focus', symbolDescription: 'Enter key'},
      {symbol: '←/→', label: 'Move backward (left) and forward (right) by one day.', symbolDescription: 'Left or right arrow keys'},
      {symbol: '↑/↓', label: 'Move backward (up) and forward (down) by one week.', symbolDescription: 'Up or down arrow keys'},
      {symbol: 'PgUp/PgDn', label: 'Switch months.', symbolDescription: 'PageUp and PageDown keys'},
      {symbol: 'Home/End', label: 'Go to the first or last day of a week.', symbolDescription: 'Home or End keys'},
      {symbol: 'Esc', label: 'Close this panel', symbolDescription: 'Escape key'},
      {symbol: '?', label: 'Open this panel', symbolDescription: 'Question mark'}
    ],
  
}

// make sure we can use it in our components
Vue.use(AirbnbStyleDatepicker, datepickerOptions)
Vue.use(VueAxios, axios)
sync(store, router)
import money from 'v-money'
 
// register directive v-money and component <money>
Vue.use(money, {precision: 2,decimal:',',thousands:'.'
})
Vue.config.productionTip = false

new Vue({
 
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
