import {
  Line,
  mixins
} from 'vue-chartjs'
const {
  reactiveProp
} = mixins
export default {

  extends: Line,
  data() {
    return {
      ingresosChart:{
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octobre', 'Novembre', 'Deciembre'],
        datasets: [
          {
            label: 'Ingresos',
            backgroundColor: '#3645d1',
            data: [0,0,0,0,0,0,0,0,0,0,0,0]
          }
        ],
        
      }
    }
  },
  methods: {
    addCommas(nStr) {
      nStr=nStr.toFixed(2);
      nStr += '';
      var x = nStr.split('.');
      var x1 = x[0];
      var x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
              x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
  }
  },
  computed: {
    options () {
      return {
        responsive: true,
        maintainAspectRatios: false,
        scales: {
          yAxes: [{
            ticks: {
              callback: (value, index, values) => {
                return this.addCommas(value);
              },
            },
          }],
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: ((tooltipItems, data) => {
              return this.addCommas(tooltipItems.yLabel)
              
            })
          }
        }
      }
    }
  },
  mixins: [reactiveProp],
  watch: {
    chartData: function () {
      this.renderChart(this.ingresosChart, this.options)
    }
  },
  data() {
    return {
      ingresosChart: {

        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octobre', 'Novembre', 'Deciembre'],
        datasets: [{
          label: 'Ingresos',
          backgroundColor: '#f87979',
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        }]
      }
    }
  },

  mounted() {

    var curr = new Date; 
    this.axios.post("http://35.231.103.64:3005/api/transacciones/ingresos-anual", {
      ano: curr.getFullYear(),
      id_eje: this.$store.getters.getUser.user.ide_eje,
    }).then((response) => {
     
      console.log("ASDSAD")
      if(response.data.length>0){
        this.ingresosChart.datasets[0].data[0] = response.data[0].imp_001
        this.ingresosChart.datasets[0].data[1] = response.data[0].imp_002
        this.ingresosChart.datasets[0].data[2] = response.data[0].imp_003
        this.ingresosChart.datasets[0].data[3] = response.data[0].imp_004
        this.ingresosChart.datasets[0].data[4] = response.data[0].imp_005
        this.ingresosChart.datasets[0].data[5] = response.data[0].imp_006
        this.ingresosChart.datasets[0].data[6] = response.data[0].imp_007
        this.ingresosChart.datasets[0].data[7] = response.data[0].imp_008
        this.ingresosChart.datasets[0].data[8] = response.data[0].imp_009
        this.ingresosChart.datasets[0].data[9] = response.data[0].imp_010
        this.ingresosChart.datasets[0].data[10] = response.data[0].imp_011
        this.ingresosChart.datasets[0].data[11] = response.data[0].imp_012
      }
     
      this.renderChart(this.ingresosChart, this.options)
    })
    
  }
}
