

import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins
export default {

  extends: Bar,
 
   mixins: [reactiveProp],
   watch: {
    chartData:function(){
      this.renderChart(this.operationsChart,this.options)
      console.log("ASDASD")
    }
    
   },
 
   data(){
    return{
      operationsChart:{
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octobre', 'Novembre', 'Deciembre'],
        datasets: [
          {
            label: 'N# de Operaciones',
            backgroundColor: '#3645d1',
            data: [0,0,0,0,0,0,0,0,0,0,0,0]
          }
        ]
      }
    }  
  },
  computed: {
    options () {
      return {
        responsive: true,
        maintainAspectRatios: false,
        scales: {
          yAxes: [{
            ticks: {
              callback: (value, index, values) => {
                return this.addCommas(value);
              },
            },
          }],
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: ((tooltipItems, data) => {
              return this.addCommas(tooltipItems.yLabel)
              
            })
          }
        }
      }
    }
  },
  methods: {
    addCommas(nStr) {
      nStr += '';
      var x = nStr.split('.');
      var x1 = x[0];
      var x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
              x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
  }
  },
  mounted () {
    var curr = new Date; // get current date
    this.axios.post("http://35.231.103.64:3005/api/transacciones/operaciones-anual",{
      ano:curr.getFullYear(),
      id_eje:this.$store.getters.getUser.user.ide_eje,
    }).then((response)=>{
    
      if(response.data.length>0){
        this.operationsChart.datasets[0].data[0]=response.data[0].imp_001
        this.operationsChart.datasets[0].data[1]=response.data[0].imp_002
        this.operationsChart.datasets[0].data[2]=response.data[0].imp_003
        this.operationsChart.datasets[0].data[3]=response.data[0].imp_004
        this.operationsChart.datasets[0].data[4]=response.data[0].imp_005
        this.operationsChart.datasets[0].data[5]=response.data[0].imp_006
        this.operationsChart.datasets[0].data[6]=response.data[0].imp_007
        this.operationsChart.datasets[0].data[7]=response.data[0].imp_008
        this.operationsChart.datasets[0].data[8]=response.data[0].imp_009
        this.operationsChart.datasets[0].data[9]=response.data[0].imp_010
        this.operationsChart.datasets[0].data[10]=response.data[0].imp_011
        this.operationsChart.datasets[0].data[11]=response.data[0].imp_012
      }

      console.log(this.options)
      
      this.renderChart(this.operationsChart,this.options)
    })
 
  }
}
