import {
  get
} from "https";

// https://vuex.vuejs.org/en/getters.html

export default {
  getUser(state) {
    return state.user
  },
  getToken(state) {
    return state.token
  }
}
