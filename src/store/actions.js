// https://vuex.vuejs.org/en/actions.html


export default {
    login({commit},user){
       
        commit('setUser',user);
        commit('setToken',token);
    },
    setUser({commit},user){
        commit('setUser',user);
    },
    destroyUser({commit}){
        commit('destroyUser')
    }
   

}
