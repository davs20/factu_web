// https://vuex.vuejs.org/en/mutations.html


export default {
    setUser(state,user){
        state.user=user
    },
    setToken(state,token){
        window.localStorage.setItem('token',token)
    },
    destroyUser(state){
        state.user=null
    }
}
